package com.twuc.webApp;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserControllerTest {
    @Test
    void should_find_book_belong_to_user_by_id() {
        int userId = 2;
        UserController userController = new UserController();
        String result = userController.findBookBelongToUser(userId);
        assertEquals("The book for user 2", result);
    }
}
