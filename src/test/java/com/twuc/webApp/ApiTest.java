package com.twuc.webApp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class ApiTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_200_when_find_book_by_user_id() throws Exception {
        mockMvc.perform(get("/api/users/2/books"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("text/plain;charset=UTF-8"))
                .andExpect(content().string("The book for user 2"));
    }

    @Test
    void should_case_sensitive() throws Exception {
        mockMvc.perform(get("/api/users/2/books/3"))
                .andExpect(status().is5xxServerError());
    }

    @Test
    void should_invoke_order_when_same_request() throws Exception {
        mockMvc.perform(get("/api/segments/good"))
                .andExpect(content().string("good"));
    }

    @Test
    void should_return_404_when_uri_contains_question_mark() throws Exception {
        mockMvc.perform(get("/api/123"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_404_when_matcher_0_char() throws Exception {
        mockMvc.perform(get("/api/"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_200_when_matcher_by_wildcard() throws Exception {
        mockMvc.perform(get("/api/wildcards/anything"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_200_when_wildcards_in_middle() throws Exception {
        mockMvc.perform(get("/api/anything/wildcards"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_500_when_matcher_error_order() throws Exception {
        mockMvc.perform(get("/api/wildcard/before/after/"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_200_when_matcher_prefix_and_suffix() throws Exception {
        mockMvc.perform(get("/api/v_wildcards.jpg"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_200_when_matcher_multistage_wildcards() throws Exception {
        mockMvc.perform(get("/api/start/wildcards/end"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_200_when_matcher_regex() throws Exception {
        mockMvc.perform(get("/api/users/2"))
                .andExpect(status().isOk())
                .andExpect(content().string("2"));
        mockMvc.perform(get("/api/users/3"))
                .andExpect(status().isOk())
                .andExpect(content().string("3"));
    }

    @Test
    void should_return_200_when_query_string_routing() throws Exception {
        mockMvc.perform(get("/api/users?id=2"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/api/users?name=3"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_url_not_contains_params_given_request_param() throws Exception {
        mockMvc.perform(get("/api/users"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_query_string_not_contains_params_given_request_param() throws Exception {
        mockMvc.perform(get("/api/users/q?id=3"))
                .andExpect(status().isOk());
    }

    @Test
    void should_return_400_when_query_string_not_contains_params_given_request_param1() throws Exception {
        mockMvc.perform(get("/api/users/q?name=3"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_400_when_query_string_not_contains_params_given_request_param2() throws Exception {
        mockMvc.perform(get("/api/users/q?id=3&name=3"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_return_200_when_bind_header() throws Exception {
        mockMvc.perform(get("/api/headers").header("name", "zijie"))
                .andExpect(status().isOk());
    }
}
