package com.twuc.webApp;

import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class UserController {

    @GetMapping("/users/{userId}/books")
    public String findBookBelongToUser(@PathVariable long userId) {
        return "The book for user " + userId;
    }

    @GetMapping("/users/{userId}/books/{bookId}")
    public String caseSensitive(@PathVariable Integer userid, @PathVariable Integer bookid) {
        return null;
    }

    @GetMapping("/segments/good")
    public String good() {
        return "good";
    }

    @GetMapping("/segments/{segmentName}")
    public String segmentName(@PathVariable String segmentName) {
        return "segmentName";
    }

    @GetMapping("/?")
    public void matcherQuestionMark() {
    }

    @GetMapping("/wildcards/*")
    public void wildcards() {
    }

    @GetMapping("/*/wildcards")
    public void wildcardsInMiddle() {
    }

    @GetMapping("/wildcard/before/*/after")
    public void errorOrder() {
    }

    @GetMapping("/v_*.jpg")
    public void matcherPrefixAndSuffix() {
    }

    @GetMapping("/**/end")
    public void matcherMultistageWildcards() {
    }

    @GetMapping("/users/{id:[\\d]}")
//    @GetMapping("/users/{id:\\d}")
    public String matcherRegex(@PathVariable int id) {
        return String.valueOf(id);
    }

    @GetMapping("/users")
    public String matcherQueryString(@RequestParam Integer id) {
        return String.valueOf(id);
    }

    @GetMapping(value = "/users/q", params = {"!name"})
    public String matcherNotPresentQueryString(@RequestParam Integer id) {
        return String.valueOf(id);
    }


    @GetMapping("/headers")
    public void bindHeaders(@RequestHeader HttpHeaders headers) {
    }

}
